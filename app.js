const http = require('http')
const port = process.env.PORT || 3000

const server = http.createServer(function(req, res){
    res.write('heroku test')
    res.end()
})

server.listen(port, function(error){
    if (error){
        console.log('error:', error)
    } else{
        console.log('server is listening on port: ', port)
    }
})